package com.ebpsource.ebplab.pdfgeneration.flyingsaucer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xhtmlrenderer.pdf.ITextRenderer;

import com.ebpsource.ebplab.pdfgeneration.common.Util;

public class FlyingSaucerExample {

    private static final Logger logger = LoggerFactory.getLogger(FlyingSaucerExample.class);
    private static final String EXAMPLE_INVOICE_TEMPLATE = "example-invoice-template.ftl";
    private static final String DEST_EXAMPLE_BIRTINGUR_PDF_FROM_STR =
            "target/birtingur-example/4804080550-flying-saucer-s.pdf";
    private static final String DEST_EXAMPLE_BIRTINGUR_PDF_FROM_FILE =
            "target/birtingur-example/4804080550-flying-saucer-f.pdf";
    private static final String DEST_EXAMPLE_INVOICE_PDF = "target/example-invoice-flying-saucer.pdf";

    public static void main(String[] args) {

        producePdfFromFreeMarkerTemplate();
        producePdfFromBirtingurXML();
    }

    public static void producePdfFromFreeMarkerTemplate() {

        String htmlDocument = transformFreeMarkerToHtml();
        createPdfFromHtmlString(htmlDocument, DEST_EXAMPLE_INVOICE_PDF);
    }

    public static void producePdfFromBirtingurXML() {

        // Create the PDF from the String
        String htmlDocument = Util.transformXMLToHTMLAsString();
        createPdfFromHtmlString(htmlDocument, DEST_EXAMPLE_BIRTINGUR_PDF_FROM_STR);

        // Create the PDF from an intermediate HTML File
        Util.transformXMLToHTMLAsFile();
        createPdfFromHtmlFile(Util.DEST_EXAMPLE_BIRTINGUR_HTML, DEST_EXAMPLE_BIRTINGUR_PDF_FROM_FILE);
    }

    protected static String transformFreeMarkerToHtml() {

        // Get the template variables
        Map<String, Object> templateVariables = Util.getTemplateVariables();

        // Process the freemarker template
        String htmlDocument = Util.processFreeMarkerTemplate(EXAMPLE_INVOICE_TEMPLATE, templateVariables);
        return htmlDocument;
    }

    protected static void createPdfFromHtmlString(String htmlString, String dest) {

        OutputStream outputStream = null;
        try {
            outputStream = new FileOutputStream(dest);

            ITextRenderer renderer = new ITextRenderer();
            renderer.setDocumentFromString(htmlString);
            renderer.layout();
            renderer.createPDF(outputStream);
        } catch (IOException e) {
            logger.error("Error creating the pdf", e);
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    logger.error("Error closing the output stream", e);
                }
            }
        }

        logger.debug("Creating the PDF: " + dest);
    }

    protected static void createPdfFromHtmlFile(String htmlFile, String dest) {

        String url;
        OutputStream outputStream = null;
        try {
            url = new File(htmlFile).toURI().toURL().toString();
            outputStream = new FileOutputStream(dest);

            ITextRenderer renderer = new ITextRenderer();
            renderer.setDocument(url);
            renderer.layout();
            renderer.createPDF(outputStream);
        } catch (MalformedURLException | FileNotFoundException e) {
            logger.error("Error creating the pdf", e);
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    logger.error("Error closing the output stream", e);
                }
            }
        }

        logger.debug("Creating the PDF: " + dest);
    }
}
