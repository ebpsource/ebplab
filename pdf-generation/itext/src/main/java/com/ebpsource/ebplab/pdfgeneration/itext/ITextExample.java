package com.ebpsource.ebplab.pdfgeneration.itext;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ebpsource.ebplab.pdfgeneration.common.Util;
import com.itextpdf.html2pdf.HtmlConverter;
import com.itextpdf.kernel.pdf.PdfWriter;

public class ITextExample {

    private static final Logger logger = LoggerFactory.getLogger(ITextExample.class);
    private static final String EXAMPLE_INVOICE_TEMPLATE = "example-invoice-template.ftl";
    private static final String DEST_EXAMPLE_BIRTINGUR_PDF_FROM_STR = "target/birtingur-example/4804080550-itext-s.pdf";
    private static final String DEST_EXAMPLE_BIRTINGUR_PDF_FROM_FILE =
            "target/birtingur-example/4804080550-itext-f.pdf";
    private static final String DEST_EXAMPLE_INVOICE_PDF = "target/example-invoice-itext.pdf";

    public static void main(String[] args) {

        producePdfFromFreeMarkerTemplate();
        producePdfFromBirtingurXML();
    }

    public static void producePdfFromFreeMarkerTemplate() {

        String htmlDocument = transformFreeMarkerToHtml();
        createPdfFromHtmlString(htmlDocument, DEST_EXAMPLE_INVOICE_PDF);
    }

    public static void producePdfFromBirtingurXML() {

        // Create the PDF from the String
        String htmlDocument = Util.transformXMLToHTMLAsString();
        createPdfFromHtmlString(htmlDocument, DEST_EXAMPLE_BIRTINGUR_PDF_FROM_STR);

        // Create the PDF from an intermediate HTML File
        Util.transformXMLToHTMLAsFile();
        createPdfFromHtmlFile(Util.DEST_EXAMPLE_BIRTINGUR_HTML, DEST_EXAMPLE_BIRTINGUR_PDF_FROM_FILE);
    }

    protected static String transformFreeMarkerToHtml() {

        // Get the template variables
        Map<String, Object> templateVariables = Util.getTemplateVariables();

        // Process the freemarker template
        String htmlDocument = Util.processFreeMarkerTemplate(EXAMPLE_INVOICE_TEMPLATE, templateVariables);
        return htmlDocument;
    }

    protected static void createPdfFromHtmlString(String htmlString, String dest) {

        try {
            PdfWriter pdfWriter = new PdfWriter(new File(dest));
            HtmlConverter.convertToPdf(htmlString, pdfWriter);
        } catch (IOException e) {
            logger.debug("Error creating the pdf", e);
        }
        logger.debug("Creating the PDF: " + dest);
    }

    protected static void createPdfFromHtmlFile(String htmlFile, String dest) {

        try {
            PdfWriter pdfWriter = new PdfWriter(new File(dest));
            FileInputStream htmlFileStream = new FileInputStream(new File(htmlFile));
            HtmlConverter.convertToPdf(htmlFileStream, pdfWriter);
        } catch (IOException e) {
            logger.debug("Error creating the pdf", e);
        }
        logger.debug("Creating the PDF: " + dest);
    }
}