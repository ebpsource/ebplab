package com.ebpsource.ebplab.pdfgeneration.fop;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.fop.apps.Fop;
import org.apache.fop.apps.FopFactory;
import org.apache.fop.apps.MimeConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

public class FOPExample {

    private static final Logger logger = LoggerFactory.getLogger(FOPExample.class);
    private static final String DEST_EXAMPLE_BIRTINGUR_PDF_FROM_FILE =
            "target/birtingur-example/4804080550-fop-f.pdf";
    private static final String EXAMPLE_BIRTINGUR_XSL = "target/birtingur-example/SICK_003.xsl";
    private static final String EXAMPLE_BIRTINGUR_XML = "target/birtingur-example/4804080550.xml";
    private static final String FOP_CONFIG = "target/classes/fop.xml";

    public static void main(String[] args) {

        producePdfFromBirtingurXML();
    }

    public static void producePdfFromBirtingurXML() {

        OutputStream outputStream = null;

        try {

            // Step 1: Construct a FopFactory by specifying a reference to the configuration file
            // (reuse if you plan to render multiple documents!)
            FopFactory fopFactory = FopFactory.newInstance(new File(FOP_CONFIG));

            // Step 2: Set up output stream.
            // Note: Using BufferedOutputStream for performance reasons (helpful with FileOutputStreams).
            outputStream =
                    new BufferedOutputStream(new FileOutputStream(new File(DEST_EXAMPLE_BIRTINGUR_PDF_FROM_FILE)));

            // Step 3: Construct fop with desired output format
            Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF, outputStream);

            // Step 4: Setup JAXP using identity transformer (without xslt)
            TransformerFactory factory = TransformerFactory.newInstance();
            // Transformer transformer = factory.newTransformer(); // identity transformer

            // Start 4 with xslt
            Source srcXslt = new StreamSource(new File(EXAMPLE_BIRTINGUR_XSL));
            Transformer transformer = factory.newTransformer(srcXslt);

            // Step 5: Setup input and output for XSLT transformation
            // Setup input stream
            // Source src = new StreamSource(new File("C:/Temp/myfile.fo"));
            Source src = new StreamSource(new File(EXAMPLE_BIRTINGUR_XML));

            // Resulting SAX events (the generated FO) must be piped through to FOP
            Result res = new SAXResult(fop.getDefaultHandler());

            // Step 6: Start XSLT transformation and FOP processing
            transformer.transform(src, res);
        } catch (IOException | TransformerException | SAXException e) {
            logger.error("Error creating the pdf", e);
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    logger.error("Error closing the output stream", e);
                }
            }
        }

        logger.debug("Creating the PDF: " + DEST_EXAMPLE_BIRTINGUR_PDF_FROM_FILE);
    }

}
