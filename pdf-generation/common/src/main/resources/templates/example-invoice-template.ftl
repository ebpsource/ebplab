<!DOCTYPE html>
<html>
<head>
<title>${title}</title>
<link type="text/css" rel="stylesheet" href="target/css/invoice.css" />
<style>
@page {
    size: 8.5in 11in;
    @
    bottom-center
    {
        content
        :
        "page "
        counter(
        page
        )
        " of  "
        counter(
        pages
        );
    }
}
</style>
</head>
<body>
    <h1 class="page-header">Blank Page</h1>
    <div style="page-break-before: always;">
        <img alt="ebpSource" src="target/images/logo-dark.png" />
        <div align="center">
          <h1>${title}</h1>
        </div>
        <table>
            <tr>
                <td><b>Name</b></td>
                <td><b>Description</b></td>
                <td><b>Quantity</b></td>
                <td><b>Cost</b></td>
            </tr>
            <#list productList as product>
            <tr>
                <td>${product.name}</td>
                <td>${product.description}</td>
                <td>${product.quantity}</td>
                <td>${product.cost}</td>
            </tr>
            </#list>
        </table>
    </div>
    <div>
        <a href="https://www.ebpsource.com/" target="_blank">ebpSource Inc.</a>
    </div>
</body>
</html>