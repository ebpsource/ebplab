<?xml version="1.0" encoding="ISO-8859-1"?>
<!--
Author: R�nar S. G.
Version: 1.0

This stylesheet outputs HTML from an XML-S file describing XML statements from www.sjukra.is
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" encoding="ISO-8859-1" indent="yes"/>
	<!-- Main templates -->
	<xsl:template match="XML-S">
		<xsl:apply-templates select="Statement"/>
	</xsl:template>
	<xsl:template match="Statement">
		<style type="text/css">
      .litill {font-family: Verdana, Helvetica, Arial, sans-serif; font-size: 7.5pt; padding-top : 4px}
      .smaerri {font-family: Verdana, Helvetica, Arial, sans-serif; font-size: 11pt; color: #000000}
      .sjukra {font-family: Rotis Semi Serif Bold, Verdana, Helvetica, Arial, sans-serif; font-size: 8pt; color: #000000}
      .smaerrinopadding {font-family: Verdana, Helvetica, Arial, sans-serif; font-size: 10pt; color: #000000; padding-top : 0px}
      .smaerripadding1 {font-family: Verdana, Helvetica, Arial, sans-serif; font-size: 9pt; color: #000000; padding-top : 5px}
      .bil{padding-top : 4pt}
  </style>
		<table width="515" border="0" height="500" style="border-style: solid; border-width: 1" cellpadding="0" cellspacing="0">
			<tr>
				<td valign="top">
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr class="litill">
							<td width="5%"/>
							<td width="41%"/>
							<td width="1%"/>
							<td width="42%"/>
							<td width="5%"/>
							<td width="6%"/>
						</tr>
						<tr class="smaerri">
							<td/>
							<td colspan="4" align="right"/>
							<td/>
						</tr>
						<tr class="sjukra">
							<td/>
							<td colspan="4">
								<br/>
								<a href="http://www.sjukra.is" target="_blank">									
								<img border="0" alt=""><xsl:attribute name="src"><xsl:value-of select="/XML-S/url/logo0"/></xsl:attribute></img>                
							    <!--img src="sjukra.jpg" border="0"/-->
								</a>
								<br/>
							</td>
							<td/>
						</tr>
						<tr class="smaerri">
							<td/>
							<td colspan="4"/>
							<td/>
						</tr>
						<tr class="sjukra">
							<td/>
							<td colspan="4">
								<br/>
								<br/>
							Velkomin/n <xsl:value-of select="Form/Nafn"/>
								<p align="justify">									
								�� hefur fengi� a�gang a� Gagnag�tt Sj�kratryggingar �slands.
								�etta er rafr�nt au�kenni �itt:
								    <br/>
									<br/>
								</p>
							</td>
							<td/>
						</tr>
						<tr>
							<td/>
							<td colspan="4">
								<table width="100%" bgcolor="#f6f6f6" border="0" cellpadding="0" cellspacing="0">
									<tr class="smaerri">
										<td colspan="6">
											<hr align="center" width="100%"/>
										</td>
									</tr>
									<tr>
										<td>
											<table width="100%" bgcolor="#f6f6f6" border="0" cellpadding="0" cellspacing="0">
												<tr>
													<td width="5%"/>
													<td width="35%"/>
													<td width="10%"/>
													<td width="30%"/>
													<td width="10%"/>
													<td width="20%"/>
												</tr>
												<xsl:apply-templates select="Form"/>
											</table>
										</td>
									</tr>
									<tr class="smaerri">
										<td/>
										<td colspan="4" align="center">
                   
                </td>
										<td> </td>
									</tr>
									<tr class="smaerri">
										<td colspan="6">
											<hr align="center" width="100%"/>
										</td>
									</tr>
								</table>
							</td>
							<td/>
						</tr>
						<tr class="smaerripadding1">
							<td> </td>
							<td colspan="4">
								<br/>
            Au�kenni� veitir ��r a�gang a� einkasv��i ��nu � vef Sj�kratrygginga �slands.
            <br/>
								<br/>
            Ef �� gleymir au�kenninu getur�u s�tt �a� aftur � �ennan sta�.
            <br/>
								<br/>
			Ef upp koma vandam�l vinsamlega haf�u samband me� �v� a� senda p�st � <u><b>gatt@sjukra.is</b></u> e�a hringja � s�ma <b>515-0007</b>.             
            <br/>
            <br/>
            <b>Me� g��ri kve�ju</b>
            <br/>
            <br/>
			<b>Starfsf�lk Sj�kratrygginga �slands.</b>
							</td>
							<td/>
						</tr>										
					</table>
				</td>
			</tr>
		</table>
	</xsl:template>
	<xsl:template match="Form">
		<tr class="smaerrinopadding">
			<td> </td>
			<td align="right">Notandanafn: </td>
			<td> </td>
			<td>
				<xsl:value-of select="Notandanafn"/>
			</td>
			<td> </td>
			<td> </td>
		</tr>
		<tr class="smaerrinopadding">
			<td> </td>
			<td align="right">Lykilor�: </td>
			<td> </td>
			<td>
				<xsl:value-of select="Lykilord"/>
			</td>
			<td> </td>
			<td> </td>
		</tr>
	</xsl:template>
</xsl:stylesheet>
