package com.ebpsource.ebplab.pdfgeneration.common;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.utility.SecurityUtilities;

public class Util {

    private static final Logger logger = LoggerFactory.getLogger(Util.class);
    private static final String TEMPLATES_PATH = "/target/templates/";
    private static final String EXAMPLE_BIRTINGUR_XSL = "target/birtingur-example/SICK_003.xsl";
    private static final String EXAMPLE_BIRTINGUR_XML = "target/birtingur-example/4804080550.xml";

    public static final String DEST_EXAMPLE_BIRTINGUR_HTML = "target/birtingur-example/4804080550.html";

    public static String processFreeMarkerTemplate(String template, Map<String, Object> variables) {

        Configuration config = new Configuration(Configuration.DEFAULT_INCOMPATIBLE_IMPROVEMENTS);
        config.setDefaultEncoding("UTF-8");

        BufferedWriter writer = null;
        String htmlStr = null;
        try {
            config.setDirectoryForTemplateLoading(
                new File(SecurityUtilities.getSystemProperty("user.dir") + TEMPLATES_PATH));

            Template tp = config.getTemplate(template);
            StringWriter stringWriter = new StringWriter();
            writer = new BufferedWriter(stringWriter);

            // Process the template, replacing variables
            tp.process(variables, writer);

            htmlStr = stringWriter.toString();
        } catch (IOException | TemplateException e) {
            logger.error("Error processing the free marker template", e);
        } finally {
            if (writer != null) {
                try {
                    writer.flush();
                    writer.close();
                } catch (IOException e) {
                    logger.error("Error flushing or closing the buffered writer", e);
                }
            }
        }

        logger.debug("Processed the freemarker template");
        return htmlStr;
    }

    public static Map<String, Object> getTemplateVariables() {

        Map<String, Object> variables = new HashMap<String, Object>(3);

        List<ProductService> products = new ArrayList<ProductService>();
        products.add(new ProductService("Milk", "2% Milk", 2, 4.99));
        products.add(new ProductService("Bread", "White Loaf of Bread", 3, 1.49));
        products.add(new ProductService("Newspaper", "The Globe and Mail", 1, 2.50));
        products.add(new ProductService("Loto Ticket", "Loto 649 Ticket with Encore", 1, 5.00));

        variables.put("title", "Another stop at Max Milk");
        variables.put("productList", products);

        logger.debug("Got the template variables");
        return variables;
    }

    public static String transformXMLToHTMLAsString() {
        TransformerFactory tFactory = TransformerFactory.newInstance();

        // Get the incoming XSLT file
        Transformer transformer = null;
        try {
            transformer = tFactory.newTransformer(new StreamSource(EXAMPLE_BIRTINGUR_XSL));
            transformer.setOutputProperty(OutputKeys.ENCODING, "ISO-8859-1");
        } catch (TransformerConfigurationException e) {
            logger.error("Error getting the XSLT file and initializing the transformer.", e);
        }

        StringWriter stringWriter = new StringWriter();
        BufferedWriter writer = new BufferedWriter(stringWriter);
        String htmlStr = null;

        // Get the XML file and apply the XSLT transformation to convert to HTML
        try {
            transformer.transform(new StreamSource(EXAMPLE_BIRTINGUR_XML), new StreamResult(writer));
            htmlStr = stringWriter.toString();
        } catch (TransformerException e) {
            logger.error("Error getting the XML file and apply the XSLT transformation to convert to HTML", e);
        } finally {
            if (writer != null) {
                try {
                    writer.flush();
                    writer.close();
                } catch (IOException e) {
                    logger.error("Error flushing or closing the buffered writer", e);
                }
            }
        }

        logger.debug("Successfully applied the XSLT transformation to the XML File and converted to HTML");
        return htmlStr;
    }

    public static void transformXMLToHTMLAsFile() {
        TransformerFactory tFactory = TransformerFactory.newInstance();

        // Get the incoming XSLT file
        Transformer transformer = null;
        try {
            transformer = tFactory.newTransformer(new StreamSource(EXAMPLE_BIRTINGUR_XSL));
            transformer.setOutputProperty(OutputKeys.ENCODING, "ISO-8859-1");
            transformer.setOutputProperty(OutputKeys.METHOD, "html");
            transformer.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, "about:legacy-compat");
        } catch (TransformerConfigurationException e) {
            logger.error("Error getting the XSLT file and initializing the transformer.", e);
        }

        // Get the XML file and apply the XSLT transformation to convert to HTML
        BufferedWriter writer = null;
        try {
            FileOutputStream fos = new FileOutputStream(new File(DEST_EXAMPLE_BIRTINGUR_HTML));
            writer = new BufferedWriter(new OutputStreamWriter(fos, StandardCharsets.ISO_8859_1));

            transformer.transform(new StreamSource(EXAMPLE_BIRTINGUR_XML), new StreamResult(fos));
        } catch (TransformerException | FileNotFoundException e) {
            logger.error("Error getting the XML file and apply the XSLT transformation to convert to HTML", e);
        } finally {
            try {
                writer.flush();
                writer.close();
            } catch (IOException e) {
                logger.error("Error flushing or closing the buffered writer", e);
            }
        }

        logger.debug("Successfully applied the XSLT transformation to the XML File and converted to HTML");
    }
}
