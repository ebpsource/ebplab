package com.ebpsource.ebplab.pdfgeneration.pdfbox;

import java.io.IOException;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PDFBoxExample {

    private static final Logger logger = LoggerFactory.getLogger(PDFBoxExample.class);

    public static void main(String[] args) {
        createHelloDocument();
    }

    private static void createHelloDocument() {
        final PDPage singlePage = new PDPage();
        final PDFont courierBoldFont = PDType1Font.COURIER_BOLD;
        final int fontSize = 12;

        try (final PDDocument document = new PDDocument()) {
            document.addPage(singlePage);
            final PDPageContentStream contentStream = new PDPageContentStream(document, singlePage);
            contentStream.beginText();
            contentStream.setFont(courierBoldFont, fontSize);
            contentStream.newLineAtOffset(150, 750);
            contentStream.showText("Hello PDFBox");
            contentStream.endText();
            contentStream.close();
            document.save("target/HelloPDFBox.pdf");
            logger.debug("Successfully generated PDF using PDFBox.");
        } catch (IOException ioEx) {
            logger.debug("Error creating document.", ioEx);
        }
    }
}