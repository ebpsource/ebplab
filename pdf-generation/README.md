# PDF Generation

Survey existing Java libraries for generating PDF content. The need is being driven by two different projects - mobile invoicing (MyBus1) and Birtingur Payments.

## Requirements

For MyBus1 the requirement is to generate PDF using a template and JSON data to resolve placeholders. We can also consider an option that uses Freemarker templates to generate an intermediate data format which is then used to generated PDF. The product roadmap for My Business (MyBus1 refers specifically to the first version) is to provide multiple templates that a user can choose, to provide options for the look and feel of their invoices.

For Birtingur Payments, the requirement is to generate PDF from xml + xsl. This might be done by first rendering HTML, and then generating PDF from the html. This requirement stems from the need to provide a PDF view of existing eDocuments in Birtingur, and recreating a view for the thousands of existing doc types is not feasible, and so the existing xml and xsl must be used as a starting point.

It might be the case that two different PDF solutions makes more sense than meeting the requirements of both projects with one. In this case, the solution supporting MyBus1 should be integrated into ebpSourceONE, and the other provided as part of the Birtingur-specific solution.

## Libraries Selected

| Library |  Version | License | Cost
|---|---|---|---|
| [iText](https://itextpdf.com/en/products/features) | 7.1.4 | Commercial | ?
| [Apache FOP](https://xmlgraphics.apache.org/fop) | 2.3 | Apache License 2.0 | $0
| [Apache PDFBox](https://pdfbox.apache.org/) | 2.0.13 | Apache License 2.0 | $0
| [Flying Saucer](https://code.google.com/archive/p/flying-saucer/) | 9.1.16 | GNU Lesser GPL | $0
| [JOD Converter](https://github.com/sbraconnier/jodconverter) | - | Apache License 2.0 | $0

### Selection Criteria

In addition to the high level requirements, consider the following criteria during the selection process:

 * Ability to control the layout of the PDF using structures such as tables, paragraphs, etc.
 * Ability to embed images and text where the text can be selected and copied
 * Ability to apply digital signatures and apply encryption
 * Ability to use character sets other then UTF-8 (e.g. ISO--8859-1 for Birtingur)

## Known Issues

### Flying Saucer with Open PDF:

 * Generates the simple PDF fine from the FreeMarker template (HTML) however it fails to generate the PDF from the Birtingur XML/XSLT file:
	 * Could investigate configuring flying saucer to use a different parser (org.xhtmlrenderer.util.Confuration to set "xr.load.xml-reader" property) where we can set the encoding encoding:
		 [http://flyingsaucerproject.github.io/flyingsaucer/r8/guide/users-guide-R8.html#xil_6](http://flyingsaucerproject.github.io/flyingsaucer/r8/guide/users-guide-R8.html#xil_6)

  ```
ERROR:  'The markup in the document following the root element must be well-formed.'
Exception in thread "main" org.xhtmlrenderer.util.XRRuntimeException: Can't load the XML resource (using TrAX transformer). org.xml.sax.SAXParseException; lineNumber: 8; columnNumber: 12; The markup in the document following the root element must be well-formed.
  ```

### Apache FOP
 
 * Generates the simple PDF fine from the FreeMarker template (HTML) however it fails to generate the PDF from the Birtingur XML/XSLT file:
	 * I believe this is saying that the XSL file would have to contain the "fo" tags so Apache FOP understands how to parse the file. In this case, the first tag the parser is expecting is the ```<fo:root>``` tag

  ```
org.apache.fop.fo.ValidationException: First element must be the fo:root formatting object. Found (Namespace URI: "", Local Name: "style") instead. Please make sure you're producing a valid XSL-FO document
  ```
  
### PDFBox
 
 * Generates the simple PDF successfully, however it is all based on absolute positioning and does not support converting html to pdf.

## References

 * [https://xebia.com/blog/comparing-apache-fop-with-itext/](https://xebia.com/blog/comparing-apache-fop-with-itext/)
 * [https://medium.com/@JustinCFarris/advanced-pdf-templating-using-xdocreport-with-jodconverter-ff092612654c](https://medium.com/@JustinCFarris/advanced-pdf-templating-using-xdocreport-with-jodconverter-ff092612654c)
 * [https://stackoverflow.com/questions/16670704/freemarker-pdf-header-footer-and-page-breaks](https://stackoverflow.com/questions/16670704/freemarker-pdf-header-footer-and-page-breaks)
