# Sign document

Select a contract document and sign it digitally.
The signature of the signer will be digitally captured and displayed on the document.
A document will be locked and only available for viewing, once it is signed.

## Getting Started

This POC, has two sample contract files to illustrate the document signing journey.

From the start page: [select and start sign](https://demo.ebpsource.com/sign-document)

1. Select a contract document from the drop-down list and 
2. Input email address, which identify the signer.
3. Click on 'sign now' button 

Will display the corresponding contract document.

Depends on whether you have signed the document the page will present signing options or display completed document.

1. If you have **NOT** signed the document, then    
The contract document display an input field and the **signing field** displayed as a 'flashing sign here' to capture your name and signature.    
  1.1. Click on the 'flashing sign here' button    
This will present you with a signature drawing box, to sign and save your signature.    
Note:- This signature will be associated with your emailId.


### Prerequisites

1. Jar dependencies     
1.1. bcprov-jdk15-1.44.jar  
1.2. iText-5.0.6.jar    
2. Update the following entries in the configuration.properties    
2.1. SIGNATURE_REPOSITORY    
The folder to store the user signature images    
2.2. CONTRACT_TEMPLATE_REPOSITORY    
The folder containing the contract templates    
2.3. WORKING_TEMPLATE_REPOSITORY    
The folder containing the working contract templates. The working contract template are the file used to create the signed contract document.     
2.4. COMPLETED_CONTRACT_REPOSITORY    
The folder containing the signed contracts.    
2.5. KEYSTORE_FILE    
The certificate keystore file

	Example configuration.properties
	
	SIGNATURE_REPOSITORY=/Users/jijoantony/dev/virginmedia/document_sign/data-store/
	CONTRACT_TEMPLATE_REPOSITORY=/Users/jijoantony/dev/virginmedia/document_sign/contract-template/
	WORKING_TEMPLATE_REPOSITORY=/Users/jijoantony/dev/virginmedia/document_sign/working-template/
	COMPLETED_CONTRACT_REPOSITORY=/Users/jijoantony/dev/virginmedia/document_sign/completed/
	//KEYSTORE_FILE=/Users/jijoantony/dev/virginmedia/iSeries_5-5/sso/keystore/Csp_keystore	
	KEYSTORE_FILE=/Users/jijoantony/dev/virginmedia/document_sign/keystore/ebpsourcecouk.jks

### Plan
		
		Phase1 (Fine tune POC for demo, with single signing document and editable fields)
		1. Reduce clicks to sign the document
		 1.a. If a user signature is already present in the server, do not display the signing box.
		 1.b. Now if users wants to change the signature they can click on the displayed sign and re-draw the signature  
		2. Fine tune the signature drawing screen (display as overlay) and maintain the aspect ratio of the signature image with available width and height. 
		  2.a. Find an appropriate size for signature box, which will work on most mobile/tablet and desktop.
		3. Make pinch zooming available on all browsers and mobile.
		4. Prepare design document for phase2 development and integration
		Phase2 (Save the document and signature to the database) allow deny signing option
		1. Rework the POC to production ready
		1.a. Follow PDF.js modular build to customise and add module to handle the view and sign PDF option.
		2. Integration with ebpSource one database
		  2.a. Create users based on their email
		  2.b. Save user's signature drawing to user profile
		  2.b. Save the signed document to the 'document' table and link to a user account
		3. Include the contract template to general configuration folder.  
		Phase3 (Include signing work flow)
		1. Include work flow to the signing process
		  1.a. Identify the signer sequence and trigger the notification based on the outcome of current step in the workflow.
		   1.a.1. Show only appropriate signature fields  to the user in the workflow.
		  1.b. Sign on multiple pages with ?one? click
  





