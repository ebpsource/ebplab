var url_part=location.href.split("?")[0];
var params =location.href.split("?")[1];

var email_param = params.split("&")[0];
var email = email_param.split("=")[1];

var contract_param =params.split("&")[1];
var contract = contract_param.split("=")[1];

var signed_param =params.split("&")[2];
var signed = signed_param.split("=")[1];
var isSigned = signed == 'true';

var reload_param =params.split("&")[3];
var reload = reload_param.split("=")[1];
var isReload = reload == 'true';

console.log('signer loaded contract[' + contract +'].'); 


var version =0;
    var orientaionChanged =false;
    var firstClick = true;
    function initializeAnotationLayer(){ 
   
    
    console.info( 'initialize anotation layer' );
    var triggeringLoadAfterClose = false;

     
   $("#signBox").on('click', function() {
   
   $("#emailId").attr('value',email);
   $("#contractName").attr('value',contract);
   
   var content = document.getElementById("signBox");
   
   if(firstClick){
     firstClick = false;   
     var signatureTab = content.querySelector(".signTab");
     $(signatureTab).attr({
	        src: '/sign-document/GetSignatureImage?emailId=' + email +"&version="+version
	    });
	
	        //go fetch the image:
     $(signatureTab).on("load", function (response, status, xhr) { 
	            if (status == "error") {
	            } else {
	             console.info('loaded the signature!');
	              
	
	            }
	        }).on( "error", function () {  
	                     
	                     if(triggeringLoadAfterClose){
	                         triggeringLoadAfterClose = false; 
	                         $(signatureTab).attr('src','../../image/sign-here.png');
	                         
	                     }else{
	                         triggeringLoadAfterClose = true; 
	                         triggerDrawSigning();
	                     }
	          } );
	          
       
    }else{
    triggeringLoadAfterClose = true; 
    triggerDrawSigning();
    } 
    
         
  
}); 

$( window ).on( "orientationchange", function( event ) {
  //orientaionChanged = true;
});
    
    }
    
    function triggerDrawSigning(){
    
    var sigCapturePage = '/sign-document/drawJSignBuildVu.html?email='+email
    
    
        $.fancybox.open({
							    src  : sigCapturePage,
							    type : 'iframe',
							    maxWidth: '640px',
							    opts : {
							      afterClose : function( instance, current ) {
							      if(orientaionChanged){
							        window.dispatchEvent(new Event('resize'));
							        console.info( 'signing drawing completed ');
									document.body.style.background = ''; 
							        window.location.replace( url_part + '?email=' + email + '&contractName=' + contract +'&signed=false&reload=true');
							       
							      }else{
							        window.dispatchEvent(new Event('resize'));
							        console.info( 'signing drawing completed ');
									 document.body.style.background = ''; 
									 version++;
									 var content = document.getElementById("signBox");
									 var signatureTab = content.querySelector("[data-field-name=signature]");
									 $(signatureTab).attr('src','/sign-document/GetSignatureImage?emailId=' + email +"&version="+version);
									 version++;
									 $("#emailId").attr('value',email);
									$("#contractName").attr('value',contract);
									 window.dispatchEvent(new Event('resize'));
									}
							      },
							      beforeShow: function() {
                					
                					//document.body.style.background = 'url(' + document.getElementById("page1").toDataURL('image/png') + ')';
                					//document.body.style.backgroundRepeat = "no-repeat";
                					//document.body.style.backgroundPosition = "top center";
							    	  this.maxWidth = '640px';
           						  },
           						  beforeLoad: function() {
           						    
           						        this.maxWidth = '640px';
           						    
           						}
							    }
							  });
    }
    

        console.info( 'signed change text'+isSigned );
    	if(isSigned){
		//	$("#completeSignLabel").html('Done');

		}
    
$(function() {
   initializeAnotationLayer();
   var content = document.getElementById("signBox");
   var signatureTab = content.querySelector("[data-field-name=signature]");
  //if reload
  if(isReload){
        firstClick = false; 
        $(signatureTab).attr({
	        src: '/sign-document/GetSignatureImage?emailId=' + email +"&version="+version
	    });
	
	        //go fetch the image:
        $(signatureTab).on("load", function (response, status, xhr) { 
	            if (status == "error") {
	            } else {
	             console.info('loaded the signature!');
	
	            }
	            $("#emailId").attr('value',email);
				$("#contractName").attr('value',contract);
	        }).on( "error", function () {  
	                       
	            $("#signature").attr('src','../../image/sign-here.png');
	                     
	          } );
	          
       
    }
 });
 