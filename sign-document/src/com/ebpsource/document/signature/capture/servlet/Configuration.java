package com.ebpsource.document.signature.capture.servlet;

import java.io.IOException;
import java.util.Properties;

public enum Configuration {
	
	SIGNATURE_REPOSITORY,
	CONTRACT_TEMPLATE_REPOSITORY,
	WORKING_TEMPLATE_REPOSITORY,
	CONTRACT_BEING_PROCESSED_REPOSITORY,
	COMPLETED_CONTRACT_REPOSITORY,
	KEYSTORE_FILE;
	private static Properties configuration;
	static{
		configuration = new Properties();
		try {
			configuration.load(Configuration.class.getResourceAsStream("/com/ebpsource/sign_document/config/configuration.properties"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public String getConfigurationValue(){
		return configuration.getProperty(this.toString());
		
	}

}
