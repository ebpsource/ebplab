package com.ebpsource.document.signature.capture.servlet;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.PdfFormField;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfSignatureAppearance;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.AcroFields.Item;



public class ViewDocument extends HttpServlet {

	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		
		
		
		System.out.println("========================================================");
		String emailId = req.getParameter("SignaturyEmail");//this is from the params
		String contractName = req.getParameter("contractName");
		System.out.println(contractName);
		
		String envelopeId = req.getParameter("envelopeId");
		System.out.println("envelopeId[" +envelopeId +"].");
		
		String impl = req.getParameter("impl");
		System.out.println("impl[" +impl +"].");
		
		String signFileName = emailId.replaceAll("[\\.,@]", "_");
		
		 String contractFileName = contractName +"_" + signFileName +"_"+ envelopeId +".pdf";	
		 
		 String completedContractFilePath = Configuration.COMPLETED_CONTRACT_REPOSITORY.getConfigurationValue() + contractFileName;
		 
		 
		 if(! new File(completedContractFilePath).exists()){
		 createContractDocument(req, contractName, contractFileName);
		 }
		
		
		boolean signed = false;
		
		resp.sendRedirect("sign/web/signer/signer.html?email=" + emailId +  "&contractName=" + contractName + "&envelopeId=" + envelopeId + "&signed=" + signed + "&reload=false&returnUrl=none");

	}
	
	private void createContractDocument(HttpServletRequest req, String contractName, String contractFileName){
		  
		   String contractBeingProcessedFilePath = Configuration.CONTRACT_BEING_PROCESSED_REPOSITORY.getConfigurationValue() + contractFileName;
		
String destinationFile = contractBeingProcessedFilePath;
		
		try{
		
		PdfReader reader = new PdfReader(Configuration.CONTRACT_TEMPLATE_REPOSITORY.getConfigurationValue() + contractName + ".pdf");
		
		
		PdfStamper pdfStamper = new PdfStamper(reader, new FileOutputStream(destinationFile),'\0');
		
		
		AcroFields form = pdfStamper.getAcroFields();
		Map<String, Item> fields = form.getFields();
		//currently handle only text fields!!!
		
		Enumeration<String> parameterNames = req.getParameterNames();
		
		Map<String,String>parameters= new HashMap<String,String>();
		
		while(parameterNames.hasMoreElements()){
			String name =parameterNames.nextElement();
			System.out.println("param key[" +name +", value[" + req.getParameter(name)+"].");
			parameters.put(name, req.getParameter(name));
		}
		
		
		req.getSession().setAttribute("params",parameters);
		for(String key : fields.keySet()){
			System.out.println("key:[" +key+"]");
			if(AcroFields.FIELD_TYPE_TEXT == form.getFieldType(key)){
				System.out.println("key:[" +key+"] is text type");
				if(key.startsWith("todays-date")){
				form.setField(key, new SimpleDateFormat("dd-MMM-yyyy").format(new Date()));	
				}else{
				form.setField(key, req.getParameter(key));
				}
				form.setFieldProperty(key, "setfflags", PdfFormField.FF_READ_ONLY, null);
			}
		}
		pdfStamper.close();
        reader.close();
		}catch(Exception exp){
			exp.printStackTrace();
		}
	}

}
