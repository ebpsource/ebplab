package com.ebpsource.document.signature.capture.servlet;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class GetSignatureImage extends HttpServlet {

    
	/**
	 * Retrieves the signature saved in the server corresponding to the given email address.
	 */
	private static final long serialVersionUID = 1L;

	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
		System.out.println("Sign image Request received");
		String emailId = request.getParameter("emailId");
		String fileName = emailId.replaceAll("[\\.,@]", "_");

        response.setContentType("image/png");

        // Set standard HTTP/1.0 no-cache header.
        response.setHeader("Pragma", "no-cache");
        
        File file = new File(Configuration.SIGNATURE_REPOSITORY.getConfigurationValue() + fileName +".png");
        
        if(!file.exists()){
        	throw new IOException("File not found[" + fileName+"].");
        }
        
        InputStream is = new FileInputStream(file);

        BufferedImage bi = ImageIO.read(is);
        OutputStream os = response.getOutputStream();
        ImageIO.write(bi, "png", os);
        System.out.println("Sign image despatched");
        
    }
}