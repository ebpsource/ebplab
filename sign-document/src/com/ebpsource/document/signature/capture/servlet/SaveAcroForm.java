package com.ebpsource.document.signature.capture.servlet;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.AcroFields.FieldPosition;
import com.itextpdf.text.pdf.AcroFields.Item;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfFormField;
import com.itextpdf.text.pdf.PdfImage;
import com.itextpdf.text.pdf.PdfIndirectObject;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfSignatureAppearance;
import com.itextpdf.text.pdf.PdfSignatureAppearance.RenderingMode;
import com.itextpdf.text.pdf.PdfStamper;

public class SaveAcroForm extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		String emailId = req.getParameter("emailId");
		String signFileName = emailId.replaceAll("[\\.,@]", "_");
		String contractName = req.getParameter("contractName");
		String envelopeId = req.getParameter("envelopeId");
		
		String contractFileName = contractName + "_" + signFileName + "_" +envelopeId +".pdf";
		String destinationFile = Configuration.COMPLETED_CONTRACT_REPOSITORY.getConfigurationValue() + contractFileName;
		
		Map<String,String> parameters = (Map<String,String>)req.getSession().getAttribute("params");
		
		System.out.println("printing parameter values stored in session.size["+parameters.size()+"]");
       for(String entry:parameters.keySet()){
			
			System.out.println("entry key[" +entry +"].");
			
		}
		
		for(Entry<String, String> entry:parameters.entrySet()){
			
			System.out.println("entry key[" +entry.getKey() +", value[" + entry.getValue()+"].");
			
		}
		
		try{
			
			
			
        Image image = Image.getInstance( Configuration.SIGNATURE_REPOSITORY.getConfigurationValue() + signFileName +".png");
        image.setBackgroundColor(BaseColor.WHITE);
       
		
		PdfReader reader = new PdfReader(Configuration.WORKING_TEMPLATE_REPOSITORY.getConfigurationValue() + contractName + ".pdf");
		
		
		
		
		PdfStamper signatureStamper = PdfStamper.createSignature(reader, new FileOutputStream(destinationFile),'\0');
		
		PdfSignatureAppearance signatureAppearance = signatureStamper.getSignatureAppearance();
		
		AcroFields form = signatureStamper.getAcroFields();
		Map<String, Item> fields = form.getFields();
		//currently handle only text fields!!
		
		
		for(String key : fields.keySet()){
			if(AcroFields.FIELD_TYPE_TEXT == form.getFieldType(key)){
				if(key.startsWith("todays-date")){
				form.setField(key, new SimpleDateFormat("dd-MMM-yyyy").format(new Date()));	
				}else{
				form.setField(key, getValue(req.getParameter(key),parameters.get(key)));
				}
				form.setFieldProperty(key, "setfflags", PdfFormField.FF_READ_ONLY, null);
			}
		}
		
		//currently handles only one signature field
		String signatureField = getSignatureFieldName(contractName);
		
		
		Rectangle signaturePosition = getSignaturePosition(contractName);
		
		Rectangle rec = new Rectangle(signaturePosition.getLeft(), signaturePosition.getLeft(),signaturePosition.getLeft(), signaturePosition.getLeft());
		
		signatureAppearance.setVisibleSignature(rec, 1, signatureField);
		
		KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
		char[] keyStorePassword = "checkfree".toCharArray();
		try(InputStream keyStoreData = new FileInputStream(Configuration.KEYSTORE_FILE.getConfigurationValue())){
		    keyStore.load(keyStoreData, keyStorePassword);
		}
		
		PrivateKey privKey = (PrivateKey) keyStore.getKey("ebpsource", "checkfree".toCharArray());
		Certificate[] certChain = keyStore.getCertificateChain("ebpsource");
		
		signatureAppearance.setCrypto(privKey, certChain, null, PdfSignatureAppearance.WINCER_SIGNED);
		signatureAppearance.setCertificationLevel(PdfSignatureAppearance.CERTIFIED_NO_CHANGES_ALLOWED);
		signatureAppearance.setSignatureGraphic(image);
		
		signatureAppearance.setLayer2Text("test");
		signatureAppearance.setRenderingMode(RenderingMode.GRAPHIC);
		//signatureAppearance.setReason("by :"+emailId );
		signatureAppearance.setAcro6Layers(true);
		
		
		
	        PdfImage stream = new PdfImage(image, "", null);
	        stream.put(new PdfName("ITXT_SpecialId"), new PdfName("123456789"));
	        PdfIndirectObject ref = signatureStamper.getWriter().addToBody(stream);
	        image.setDirectReference(ref.getIndirectReference());
	        image.setScaleToFitLineWhenOverflow(true);
	        image.setAbsolutePosition(signaturePosition.getLeft(), signaturePosition.getBottom());
	        image.scaleToFit(110, 36);
	        
	        //image.scaleToFit(signaturePosition.getWidth(), signaturePosition.getHeight());
	        
	        PdfContentByte over = signatureStamper.getOverContent(1);
	        over.addImage(image);
		
		
		
		signatureStamper.close();
        reader.close();
		 
		}catch(Exception exp){
			exp.printStackTrace();
		}
		removeFileFromBeingProcessed(contractFileName);
		String returnUrl = parameters.get("returnUrl");
		resp.sendRedirect("sign/web/signer/signer.html?email=" + emailId +  "&contractName=" + contractName +"&envelopeId="+envelopeId+"&signed=true&reload=false&returnUrl="+returnUrl);
		//String returnUrl = parameters.get("returnUrl");
		//resp.sendRedirect(returnUrl);
		
	}

	private void removeFileFromBeingProcessed(String contractFileName) {
		String destinationFilePath = Configuration.CONTRACT_BEING_PROCESSED_REPOSITORY.getConfigurationValue() + contractFileName;
		File beingProcessedFile = new File(destinationFilePath);
		if(beingProcessedFile.exists()){
			if(beingProcessedFile.canRead() &&beingProcessedFile.canWrite()){
			beingProcessedFile.delete();
			}
		}
		
	}

	private String getValue(String parameter, String values) {
		String result =parameter;
		if(parameter==null){
			
				result = values;
		}
		return result;
	}

	

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(req, resp);
	}
	
	
	private Rectangle getSignaturePosition(String contractName) throws IOException{
		
		PdfReader reader = new PdfReader(Configuration.CONTRACT_TEMPLATE_REPOSITORY.getConfigurationValue() + contractName + ".pdf");
		AcroFields form = reader.getAcroFields();
		
		 Rectangle result = null;
		
		 for(String signame : form.getBlankSignatureNames()) {
			 
			 java.util.List<FieldPosition> positions = form.getFieldPositions(signame);
		      
			 result = positions.get(0).position; // In points:

		      break;
			
		 }
		 reader.close();
		return result;
		
	}
	
private String getSignatureFieldName(String contractName) throws IOException{
		
		PdfReader reader = new PdfReader(Configuration.CONTRACT_TEMPLATE_REPOSITORY.getConfigurationValue() + contractName + ".pdf");
	
		
		AcroFields form = reader.getAcroFields();
		String signatureField ="";
		 for(String signame : form.getBlankSignatureNames()) {
			 signatureField= signame;
			
		 }
		
		 reader.close(); 
		
		return signatureField;
		
	}


}
