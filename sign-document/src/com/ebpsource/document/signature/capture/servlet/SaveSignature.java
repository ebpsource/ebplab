package com.ebpsource.document.signature.capture.servlet;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.Base64;

public class SaveSignature extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		System.out.println("========================================================");
		String emailId = req.getParameter("emailId");
		String signatureImage = req.getParameter("signatureImage");
		
		System.out.println(signatureImage);
		
		signatureImage = signatureImage.substring("data:image/png;base64,".length());
		
		byte[] signatureImageByte = decode(signatureImage);
		
		ByteArrayInputStream bis = new ByteArrayInputStream(signatureImageByte);
		
		BufferedImage bi = ImageIO.read(bis);
		
		try {
			BufferedImage newBi = changePngTransparentColor(bi, Color.WHITE);
			String fileName = emailId.replaceAll("[\\.,@]", "_");
			
			File file = new File(Configuration.SIGNATURE_REPOSITORY.getConfigurationValue()+fileName +".png");
			
			ImageIO.write(newBi, "png", file);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	private byte[] decode(String imageData) throws IOException {
		
		byte[] data = Base64.getDecoder().decode(imageData);
		for (int i = 0; i <data.length; ++i) {
		if (data[i] <0) {
		//Adjust the abnormal data 
		data[i] += 256;
		}
		}
		return data;
		}
	
	
public static BufferedImage changePngTransparentColor(BufferedImage bufferedImage, Color color) throws Exception{
        
        int count = 0;
        int countNot = 0;
        BufferedImage bi = new BufferedImage(bufferedImage.getWidth(),bufferedImage.getHeight(),BufferedImage.TYPE_INT_RGB);
        for (int x=0;x<bufferedImage.getWidth();x++){
            for (int y=0;y<bufferedImage.getHeight();y++){
                int rgba = bufferedImage.getRGB(x,y);
                boolean isTrans = (rgba & 0xff000000) == 0;
                if (isTrans){
                    bi.setRGB(x,y, (color.getRGB()));
                    count ++;
                } else {
                    countNot++;
                    bi.setRGB(x,y,rgba);
                }
            }
        }
        System.out.println("Count "+count+"/"+countNot);
       
        return bi;
    }


}
