package com.ebpsource.document.signature.capture.servlet;

import java.io.File;
import java.io.FileInputStream;

import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class PickDocument extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		
		
		
		System.out.println("========================================================");
		String emailId = req.getParameter("emailId");
		String contractName = req.getParameter("contractName");
		System.out.println(contractName);
		
		String envelopeId = req.getParameter("envelopeId");
		System.out.println("envelopeId[" +envelopeId +"].");
		
		String signFileName = emailId.replaceAll("[\\.,@]", "_");
		
	   String contractFileName = contractName +"_" + signFileName +"_" + envelopeId+".pdf";	
	   String completedContractFilePath = Configuration.COMPLETED_CONTRACT_REPOSITORY.getConfigurationValue() + contractFileName;
	   String contractBeingProcessedFilePath = Configuration.CONTRACT_BEING_PROCESSED_REPOSITORY.getConfigurationValue() + contractFileName;
	   
	   String contractTemplateFileName =  contractName +".pdf";
	   String contractTemplateFilePath = Configuration.CONTRACT_TEMPLATE_REPOSITORY.getConfigurationValue() + contractTemplateFileName;
		
	   File contractBeingProcessedFile = new File(contractBeingProcessedFilePath);
		File contractFile = new File(completedContractFilePath);
		String pdfFileName = contractFileName;
		
		if(!contractFile.exists()){
			if(contractBeingProcessedFile.exists()){
				contractFile = new File(contractBeingProcessedFilePath);
				
			}else{
			contractFile = new File(contractTemplateFilePath);
			pdfFileName = contractTemplateFileName;
			}
		}
		
		resp.setContentType("application/pdf");
		resp.addHeader("Content-Disposition", "inline; filename=" + pdfFileName);
		resp.addHeader("fileName", pdfFileName);
		resp.setContentLength((int) contractFile.length());
		
		FileInputStream fileInputStream = new FileInputStream(contractFile);
		OutputStream responseOutputStream = resp.getOutputStream();
		int bytes;
		while ((bytes = fileInputStream.read()) != -1) {
			responseOutputStream.write(bytes);
		}
		fileInputStream.close();
		
	}

}
